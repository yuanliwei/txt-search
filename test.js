const search = require('./index')

async function start() {
  
    let query = process.argv[2] || '李白'

    let result = search(query)

    console.log(result);

}

start()